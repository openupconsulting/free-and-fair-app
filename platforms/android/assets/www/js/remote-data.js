var remoteData = {
		
	tables: { },
	    
	loadData: function(accessToken, successCallback, errorCallback) {
        remoteData.loadEmail(accessToken, function() {
            remoteData.loadDoc(accessToken, function(uri) {
                remoteData.loadTables(accessToken, uri, function(tables) {
                    var ballotPromise = remoteData.loadBallotDetails(accessToken);
                    var sitesPromise = remoteData.loadVotingSites(accessToken);
                    var textsPromise = remoteData.loadAppTexts(accessToken);
                    
                    $.when(ballotPromise, sitesPromise, textsPromise)
                    .then(successCallback, errorCallback);
                }, errorCallback);
            }, errorCallback);
        }, errorCallback);
	},
	
    loadEmail: function(accessToken, callback, errorCallback) {
		$.ajax({
			type: "GET",
			url: "https://www.googleapis.com/userinfo/email?alt=json",
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0" 
			}
		}).done(function(data) {
            localStorage.user_email = data.data.email;
            callback();
		}).fail(function(response) {
            errorCallback();
		});		
    },

	loadDoc: function(accessToken, callback, errorCallback) {
		$.ajax({
			type: "GET",
			url: "https://spreadsheets.google.com/feeds/spreadsheets/private/full",
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0" 
			}
		}).done(function(data) {
//            console.log("TOKEN  " + accessToken);
			$(data).find("entry").each(function() {
				var title = $(this).find("title").text();
				if (title == "ElectionsMonitor") {
					var tablesUri = $(this).find("content").first().attr("src");
					callback(tablesUri);
				}
			});
		}).fail(function(response) {
            errorCallback();
		});		
	},
	
	loadTables: function(accessToken, spreadsheetUri, callback, errorCallback) {
		$.ajax({
			type: "GET",
			url: spreadsheetUri,
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			tables = { };

            var noonPromise;
            var closingPromise;
            var finalPromise;
            
			$(data).find("entry").each(function() {				
				var title = $(this).find("title").text();
				var uri = $(this).find("content").attr("src");

                if (title === "Noon Participation") {
                    noonPromise = remoteData.loadPostUri(accessToken, uri, "noon_participation");                    
                }

                if (title === "Closing Participation") {
                    closingPromise = remoteData.loadPostUri(accessToken, uri, "closing_participation");                    
                }

                if (title === "Final results") {
                    finalPromise = remoteData.loadPostUri(accessToken, uri, "final_results");                    
                }
                
				tables[title] = uri;
			});
			
            $.when(noonPromise, closingPromise, finalPromise).then(function() {
                callback(tables);
            });
		}).fail(function(response) {
            errorCallback();
        });
	}, 
	
    loadPostUri: function(accessToken, uri, localStorageKey) {
        var deferred = $.Deferred();
        
        $.ajax({
			type: "GET",
			url: uri,
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			$(data).find("link").each(function() {	
                if ($(this).attr("rel") === "http://schemas.google.com/g/2005#post") {
                    localStorage[localStorageKey] = $(this).attr("href");
                }
			});
			
			deferred.resolve();
		}).fail(function(response) {
            deferred.reject();
		});
        
        return deferred.promise();
    },
    
	loadBallotDetails: function(accessToken) {
		var deferred = $.Deferred();

		$.ajax({
			type: "GET",
			url: tables["Ballot details"],
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			var ballotDetails = [];

			$(data).find("entry").each(function() {				
				var listName = $(this).find("listname").text();
				var listId = $(this).find("listid").text();
				ballotDetails.push({ id: listId, name: listName });
			});
			
			var ballotPromise = localDataSaveBallotDetails(ballotDetails);
			$.when(ballotPromise).then(deferred.resolve, deferred.reject);
		}).fail(function(response) {
			deferred.reject();
		});
		
		return deferred.promise();
	}, 
	
	loadVotingSites: function(accessToken) {
		var deferred = $.Deferred();

		$.ajax({
			type: "GET",
			url: tables["Voting Sites"],
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			var votingSites = [];
			
			$(data).find("entry").each(function() {				
				var site = { };
				
                site.id = $(this).find("siteid").text();
				site.name = $(this).find("name").text();
				site.address = $(this).find("address").text();
				site.longitude = $(this).find("longitude").text();
				site.latitude = $(this).find("latitude").text();
				site.zipcode = $(this).find("zipcode").text();
				site.city = $(this).find("city").text();
				site.state = $(this).find("state").text();
				site.registeredvoters = $(this).find("registeredvoters").text();
				
				votingSites.push(site);
			});
			
            var sitePromise = localDataSaveVotingSites(votingSites);
			$.when(sitePromise).then(deferred.resolve, deferred.reject);
		}).fail(function(response) {
			deferred.reject();
		});

		return deferred.promise();	
	},
	
	loadAppTexts: function(accessToken) {
		var deferred = $.Deferred();
		
		$.ajax({
			type: "GET",
			url: tables["App Text"],
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			var appTexts = [];
			
			$(data).find("entry").each(function() {				
				var txt = { };
				
				txt.key = $(this).find("key").text();
				txt.value = $(this).find("value").text();
				
				appTexts.push(txt);
			});

            var appTextsPromise = localDataSaveAppTexts(appTexts);
			$.when(appTextsPromise).then(deferred.resolve, deferred.reject);
		}).fail(function(response) {
			deferred.reject();
		});
		
		return deferred.promise();
	}, 

    saveResults: function(accessToken, url, body) {
        var d = new Date();
        
        var body = body.replace('##TIMESTAMP##', reportData.formatDate(d));
                
		var deferred = $.Deferred();

        $.ajax({
			type: "POST",
			url: url,
			headers: {
				'Authorization': "Bearer " + accessToken,
                'Content-type': "application/atom+xml"
			},
            data: body
		}).done(function(data) {
            deferred.resolve();
		}).fail(function(response) {
			deferred.reject();
		});
		
		return deferred.promise();   
    }
    
}
