$(document).on("pageinit", "#page-final-results", function(event) { 	
    
    var toSend;
            
    $('#submit-final-results').bind('mousedown', function(e) {
        var votes = {};
        
        $("label").each(function() {
            var listName = $(this).attr("for");
            var votesForList = $("#" + listName).val();                        
            votes[listName] = votesForList;
        });              
        
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
    
        googleapi.getToken(config.authOptions).done(function(data) {
            toSend = reportData.prepareFinalResults(votes);
            var promise = remoteData.saveResults(data.access_token, localStorage.final_results, toSend);
            $.when(promise).then(dataSent, dataSendFailed);
        }).fail(function(data) {
            dataSendFailed();
        });
    });
    
    function dataSent() {
        $.mobile.loading("hide");
        $.mobile.changePage("#page-final-results-capture", { transition: "none" });
    }
    
    function dataSendFailed() {
        localDataSaveUnsentResults(localStorage.final_results, toSend);
        $.mobile.loading("hide");
        $.mobile.changePage("#page-final-results-capture", { transition: "none" });
    }	
    
});

$(document).on("pageshow", "#page-final-results", function(event) { 	
		
    $("#page-final-results-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);

	var ballotPromise = localDataLoadBallotDetails();
	
	$.when(ballotPromise).then(dataLoaded, dataLoadFailed);

	function dataLoaded(results) {
		var items = "";
        $.each(results, function(index, list) {
            items += '<label for="list' + list.id + '">' + list.id + '. ' + list.name + '</label>';
            items += '<input type="number" name="list' + list.id 
                    + '" id="list' + list.id + '" value=""/>';
        });
	
        $("#final-results-container").html(items);
        $("#final-results-container").trigger("create");
	}
	
	function dataLoadFailed() {
        $("#final-data-load-error").popup("open", { transition: "none", position: "window" });
	}

    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });

});
