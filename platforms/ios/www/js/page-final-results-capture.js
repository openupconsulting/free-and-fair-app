$(document).on("pageinit", "#page-final-results-capture", function(event) { 	
   
    $('#final-results-capture-done').bind('mouseup', function(e) {
        $.mobile.changePage("#page-task", { transition: "none" });        
    });

    $.when(localDataLoadAppText("email results")).then(function(text) {
        $('#send-final-results-via-email').attr('href', "mailto:" + text + "?subject=Final%20results%20-%20" 
                                       + encodeURIComponent(reportData.votingSite.id + ' ' + reportData.votingSite.name));
    });

    $('#take-final-results-photo').bind('mouseup', function(e) {
        navigator.camera.getPicture(onSuccess, onFail, { 
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URL,
            saveToPhotoAlbum: true
        }); 
    });
        
    function onSuccess(imageData) {
    }
    
    function onFail(message) {
    }	
});

$(document).on("pageshow", "#page-final-results-capture", function(event) { 

    $("#page-final-results-capture-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);
    
    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });

});
