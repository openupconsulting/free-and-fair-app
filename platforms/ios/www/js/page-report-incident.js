$(document).on("pageinit", "#page-report-incident", function (event) { 	

    $('#report-incident-done').bind('mouseup', function(e) {
        $.mobile.changePage("#page-task", { transition: "none" });        
    });

    $.when(localDataLoadAppText("email incident")).then(function (text) {
        $('#send-incident-via-email').attr('href', "mailto:" + text + "?subject=Report%20incident%20-%20" 
                                       + encodeURIComponent(reportData.votingSite.id + ' ' + reportData.votingSite.name));
    });

    $('#take-incident-photo').bind('mouseup', function (e) {
        navigator.camera.getPicture(onSuccess, onFail, { 
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URL,
            saveToPhotoAlbum: true
        }); 
    });
        
    function onSuccess(imageData) {
    }
    
    function onFail(message) {
    }	
});


$(document).on("pageshow", "#page-report-incident", function (event) {
    
    $("#page-report-incident-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);
  
    navigator.geolocation.getCurrentPosition(function (position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });

});
