var googleapi = {
		
    authorize: function(options) {
        var deferred = $.Deferred();
        
    	var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
    		client_id: options.client_id,
    	    redirect_uri: options.redirect_uri,
    	    response_type: 'code',
    	    scope: options.scope,
    	    access_type: 'offline'
    	});

    	var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');
    	
        authWindow.addEventListener('loadstart', loadstartAuth);
        authWindow.addEventListener('exit', exitAuth);  

        var code;
        var error;
        
    	function loadstartAuth(e) {
    		var url = e.url;
    		
            code = /\?code=(.+)$/.exec(url);
    		error = /\?error=(.+)$/.exec(url);

            if (code || error) {
                authWindow.close();
    		}    		
    	}
    	
        function exitAuth() {
    		if (code) {
    			$.post('https://accounts.google.com/o/oauth2/token', {
    				code: code[1],
    				client_id: options.client_id,
    				client_secret: options.client_secret,
    				redirect_uri: options.redirect_uri,
    				grant_type: 'authorization_code'
    			}).done(function(data) {
        			googleapi.setToken(data); //Remember I said we need to call this to cache the token?
    				deferred.resolve(data);
    			}).fail(function(response) {
    				deferred.reject(response);
    			});
			} else if (error) {
				deferred.reject({
					error: error[1]
				});
			}		              
        }
        
		return deferred.promise();
    },

    getToken: function(options) {
    	var deferred = $.Deferred();
    	var now = new Date().getTime();

    	if (now < localStorage.expires_at) {
    		//The token is still valid, so immediately return it from the cache
    		deferred.resolve({
    			access_token: localStorage.access_token
    		});
    	} else if (localStorage.refresh_token != null && localStorage.refresh_token != "undefined") {
    		//The token is expired, but we can get a new one with a refresh token
    		$.post( 'https://accounts.google.com/o/oauth2/token', {
    			refresh_token: localStorage.refresh_token,
    			client_id: options.client_id,
    			client_secret: options.client_secret,
    			grant_type: 'refresh_token'
    		}).done(function(data) {
    			googleapi.setToken(data); //Remember I said we need to call this to cache the token?
    			deferred.resolve(data);
    		}).fail(function(response) {
    			deferred.reject(response);
    		});
    	} else {
    		googleapi.authorize(options).done(function(data) {
    			deferred.resolve(data);
    		}).fail(function(response) {
    			deferred.reject(response);
    		});
    	}

        return deferred.promise();
	},

	setToken: function(data) {
		localStorage.access_token = data.access_token;
		localStorage.refresh_token = data.refresh_token || localStorage.refresh_token;
		
		//Calculate exactly when the token will expire, then subtract
		//one minute to give ourselves a small buffer.
		var now = new Date().getTime();
		var expiresAt = now + parseInt(data.expires_in, 10) * 1000 - 60000;
		localStorage.expires_at = expiresAt;
	}
	
};