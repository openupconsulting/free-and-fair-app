$(document).on("pageinit", "#page-noon-participation", function(event) { 
    
    var toSend;
        
    $('#submit-noon-participation').bind('mousedown', function(e) {
        var votes = $("#result-noon-participation").val();
        
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
    
        googleapi.getToken(config.authOptions).done(function(data) {
            toSend = reportData.prepareNoonParticipationResults(votes);            
            var promise = remoteData.saveResults(data.access_token, localStorage.noon_participation, toSend);
            $.when(promise).then(dataSent, dataSendFailed);
        }).fail(function(data) {
            dataSendFailed();
        });
    });
    
    function dataSent() {
        $.mobile.loading("hide");
        $.mobile.changePage("#page-task", { transition: "none" });        
    }
    
    function dataSendFailed() {
        localDataSaveUnsentResults(localStorage.noon_participation, toSend);
        $.mobile.loading("hide");   
        $.mobile.changePage("#page-task", { transition: "none" });        
    }	
    
});

$(document).on("pageshow", "#page-noon-participation", function(event) { 
    
    $("#page-noon-participation-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);

    $("#result-noon-participation").val("");

    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });
    
});